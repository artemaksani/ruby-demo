# -*- encoding: utf-8 -*-
# stub: headless 0.2.2 ruby lib

Gem::Specification.new do |s|
  s.name = "headless".freeze
  s.version = "0.2.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Leonid Shevtsov".freeze]
  s.date = "2011-08-31"
  s.description = "    Headless is a Ruby interface for Xvfb. It allows you to create a headless display straight from Ruby code, hiding some low-level action.\n".freeze
  s.email = "leonid@shevtsov.me".freeze
  s.homepage = "http://leonid.shevtsov.me/en/headless".freeze
  s.requirements = ["Xvfb".freeze]
  s.rubygems_version = "2.5.2.3".freeze
  s.summary = "Ruby headless display interface".freeze

  s.installed_by_version = "2.5.2.3" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 3

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rspec>.freeze, ["~> 2.6"])
    else
      s.add_dependency(%q<rspec>.freeze, ["~> 2.6"])
    end
  else
    s.add_dependency(%q<rspec>.freeze, ["~> 2.6"])
  end
end
