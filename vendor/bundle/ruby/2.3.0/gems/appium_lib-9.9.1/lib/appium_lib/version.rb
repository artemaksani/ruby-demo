module Appium
  # Version and Date are defined on the 'Appium' module, not 'Appium::Common'
  VERSION = '9.9.1'.freeze unless defined? ::Appium::VERSION
  DATE    = '2018-02-02'.freeze unless defined? ::Appium::DATE
end
