module Appium
  module Core
    VERSION = '1.3.8'.freeze unless defined? ::Appium::Core::VERSION
    DATE    = '2018-04-12'.freeze unless defined? ::Appium::Core::DATE
  end
end
