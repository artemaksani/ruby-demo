Given(/^I launch https:\/\/openbet\.tenrox\.net$/) do
  @LoginPage = LoginPage.new(@browser)
  @LoginPage.visit
end

And(/^I enter username$/) do
  @LoginPage.enterUsername("xxxxxxx")
end

And(/^I enter password$/) do
  @LoginPage.enterPassword("yyyyyyy")
end

When(/^I click Login button$/) do
  @LoginPage.clickLoginButton
end

Then(/^I see Error Message$/) do
  @LoginPage.verifyErrorMessage
end