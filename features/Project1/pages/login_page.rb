class LoginPage
  attr_accessor :loginTab,:txtUsername,:txtPassword,:btnLogin, :errorMsg

  def initialize(browser)
    @browser = browser
    @txtUsername = @browser.text_field(:id => "E_UserName")
    @txtPassword = @browser.text_field(:id => "E_Password")
    @btnLogin      = @browser.element(:id => "btnLogon")
    @errorMsg    = @browser.element(:id=>"ErrorMessage")
  end

  def visit
    @browser.goto "https://openbet.tenrox.net/TEnterprise/Core/Base/Logon.aspx?orgname=OpenBet"
  end

  def enterUsername(username)
    @txtUsername.set username
  end

  def enterPassword(password)
    @txtPassword.set password
  end

  def clickLoginButton
    @btnLogin.click
  end

  def verifyErrorMessage()
    @errorMsg.wait_until_present
  end
end